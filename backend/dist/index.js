"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
require("dotenv/config");
const mongoose_1 = __importDefault(require("mongoose"));
const MyUserRoute_1 = __importDefault(require("./routes/MyUserRoute"));
// Connect to MongoDB database using the connection string from environment variables
mongoose_1.default
    .connect(process.env.MONGODB_CONNECTION_STRING) // Connect to MongoDB database
    .then(() => console.log("Connected to database!")); // Log successful database connection
// Initialize Express application
const app = (0, express_1.default)(); // Create Express app
// Configure middleware
app.use(express_1.default.json()); // Parse JSON bodies in requests
app.use((0, cors_1.default)()); // Enable CORS for all routes
// Define a test route to check if the server is running
/* app.get("/test", async (req: Request, res: Response) => {
  res.json({ message: "Hello!" });
}); */
app.get("health", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.send({ message: "health OK!" });
}));
// Mount user route module under the "/api/my/user" endpoint
app.use("/api/my/user", MyUserRoute_1.default); // Delegate requests with path starting with "/api/my/user" to the user route module
// Start the server and listen on port 7000
app.listen(7000, () => {
    console.log("Server started on localhost:7000");
});
