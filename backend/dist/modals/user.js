"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Import the Mongoose library
const mongoose_1 = __importDefault(require("mongoose"));
// Define the schema for the user
const userSchema = new mongoose_1.default.Schema({
    // Define a field for the auth0Id, which is a string and is required
    auth0Id: {
        type: String,
        required: true,
    },
    // Define a field for the email, which is a string and is required
    email: {
        type: String,
        required: true,
    },
    // Define a field for the name, which is an object containing addressLine1, city, and country
    name: {
        type: String,
    },
    addressLine1: {
        type: String,
    },
    country: {
        type: String,
    },
    city: {
        type: String,
    },
});
// Create a Mongoose model named "User" using the defined schema
const User = mongoose_1.default.model("User", userSchema);
// Export the User model to be used in other parts of the application
exports.default = User;
